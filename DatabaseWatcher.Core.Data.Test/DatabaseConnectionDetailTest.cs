﻿//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace DatabaseWatcher.Core.Data.Test
{
  using System;
  using NUnit.Framework;

  /// <summary>
  /// database connection detail tests.
  /// </summary>
  [TestFixture]
  public class DatabaseConnectionDetailTest
  {
    /// <summary>
    /// Constructors the defaults test.
    /// </summary>
    [Test]
    public void ConstructorDefaultsTest()
    {
      var test = new DatabaseConnectionDetail();
      Assert.IsNotNull(test.CreatedDate, "CreationDate is null and shouldn't be.");
      Assert.IsTrue(test.ConnectionNumber == 0, "The default connection number is not 0.");
      Assert.IsNotNull(test.Username, "The default username is null and shouldn't be.");
      Assert.IsTrue(string.IsNullOrEmpty(test.Username), "The default username is not empty and it should be.");
      Assert.IsNotNull(test.ConnectionMetaData, "The meta data for the connection is null and shouldn't be.");
    }

    /// <summary>
    /// Test that the constructor sets the date correctly in the object created.
    /// </summary>
    [Test]
    public void ConstructorSetsDate()
    {
      var testDate = DateTime.Now;
      var testConnection = new DatabaseConnectionDetail();

      Assert.IsNotNull(testConnection.CreatedDate, "The created date is null and should not be");
      Assert.IsTrue(DateTime.Compare(testDate, testConnection.CreatedDate) < 1, string.Format("The created date [{0}] is before the date captured before the object was created [{1}]", testConnection.CreatedDate.ToString("O"), testDate.ToString("O")));
    }

    /// <summary>
    /// Test that the constructor of the connection detail produces a different date each time.
    /// </summary>
    [Test]
    public void ConstructorSetsDifferentDate()
    {
      var testOne = new DatabaseConnectionDetail();
      var testTwo = new DatabaseConnectionDetail();
      Assert.IsFalse(testOne.CreatedDate == testTwo.CreatedDate, "The created date of both test connection detail objects is the same");
    }

    /// <summary>
    /// Test that the connection number can be set.
    /// </summary>
    [Test]
    public void ConnectionNumberCanBeSet()
    {
      const long TestValue = 123;

      var test = new DatabaseConnectionDetail();

      Assert.IsFalse(test.ConnectionNumber == TestValue, "The connection number was already set to the test value by default???");

      test.ConnectionNumber = TestValue;

      Assert.IsTrue(test.ConnectionNumber == TestValue, string.Format("Expected the value of the connection number to be [{0}] but it was [{1}]", TestValue, test.ConnectionNumber));
    }

    /// <summary>
    /// Test that the connection number cannot be negative
    /// </summary>
    [Test]
    public void ConnectionNumberCannotBeNegative()
    {
      var test = new DatabaseConnectionDetail();
      test.ConnectionNumber = -1;
      Assert.IsFalse(test.ConnectionNumber < 0, "The connection number was negative and should never be.");
    }

    /// <summary>
    /// Test that the username can be set
    /// </summary>
    [Test]
    public void UsernameCanBeSet()
    {
      const string TestValue = "alfred";

      var test = new DatabaseConnectionDetail();
      Assert.IsTrue(string.IsNullOrEmpty(test.Username), "The username wasn't empty at construction");
      test.Username = TestValue;
      Assert.IsTrue(TestValue.Equals(test.Username), string.Format("Expected the username to be [{0}] but it was [{1}].", TestValue, test.Username));
    }

    /// <summary>
    /// Check that the username cannot be set to null.
    /// </summary>
    [Test]
    public void UsernameCannotBeNull()
    {
      var test = new DatabaseConnectionDetail();
      Assert.IsNotNull(test.Username, "Username default value was null and it shouldn't be");
      test.Username = null;
      Assert.IsNotNull(test.Username, "Username was null and that shouldn't be allowed.");
    }

    /// <summary>
    /// Test that the default connection meta data is not null.
    /// </summary>
    [Test]
    public void DefaultConnectionMetaDataIsNotNull()
    {
      var test = new DatabaseConnectionDetail();
      Assert.IsNotNull(test.ConnectionMetaData, "The connection meta data is null and shouldn't be");
    }

    /// <summary>
    /// Test that the meta data cannot be set to null.
    /// </summary>
    [Test]
    public void MetaDataCannotBeSetToNull()
    {
      var test = new DatabaseConnectionDetail();
      test.ConnectionMetaData = null;
      Assert.IsNotNull(test.ConnectionMetaData, "The connection meta data is null and shouldn't be");
    }

  }
}
