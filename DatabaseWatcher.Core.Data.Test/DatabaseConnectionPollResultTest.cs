﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Data.Test
{
  using System;
  using NUnit.Framework;

  /// <summary>
  /// Database connection poll result tests.
  /// </summary>
  [TestFixture]
  public class DatabaseConnectionPollResultTest
  {
    /// <summary>
    /// Test the poll result has a read only taken time.
    /// </summary>
    [Test]
    public void PollResultHasAReadOnlyTakenTime()
    {
      var test = new DatabaseConnectionPollResult("test");
      var testTime = DateTime.Now;

      Assert.IsNotNull(test.TimeTaken, "The time taken value is null and should not be");
      Assert.IsFalse(DateTime.MinValue.Equals(test.TimeTaken), "The time taken is populated with the minimum value");
      Assert.IsTrue(testTime > test.TimeTaken, string.Format("The time taken [{0}] is not before the time taken after the object creation [{1}]", test.TimeTaken, testTime));
    }

    /// <summary>
    /// Test the poll result has a database name from the constructor.
    /// </summary>
    [Test]
    public void PollResultHasADatabaseNameFromTheConstructor()
    {
      const string DatabaseName = "TestDatabaseName";
      var test = new DatabaseConnectionPollResult(DatabaseName);
      Assert.IsTrue(test.DatabaseName.Equals(DatabaseName), string.Format("Expected the database name to be [{0}] but it was [{1}]", DatabaseName, test.DatabaseName));
    }

    /// <summary>
    /// Test the poll result throws an argument exception when the database name is not populated.
    /// </summary>
    [TestCase(null)]
    [TestCase("")]
    [TestCase("           ")]
    public void PollResultThrowsAnArgumentExceptionWhenDatabaseNameIsNotPopulated(string value)
    {
      Assert.Throws<ArgumentException>(() => new DatabaseConnectionPollResult(null), string.Format("No exception was thrown when [{0}] was given as the database name", value ?? "null"));
    }
    /// <summary>
    /// Test the polls result database name has no space padding.
    /// </summary>
    [TestCase("test   ", "test")]
    [TestCase("   test", "test")]
    [TestCase("       test   ", "test")]
    public void PollResultDatabaseNameHasNotSpacePadding(string name, string result)
    {
      var test = new DatabaseConnectionPollResult(name);
      Assert.IsTrue(test.DatabaseName.Equals(result), string.Format("Expected the database name to be [{0}] but it was [{1}]", result, test.DatabaseName));
    }

    /// <summary>
    /// Test the poll result has a non null details list.
    /// </summary>
    [Test]
    public void PollResultHasANonNullDetailsList()
    {
      var test = new DatabaseConnectionPollResult("test");
      Assert.IsNotNull(test.ConnectionDetails, "The connection details are null and shouldn't be");
    }
    /// <summary>
    /// Test that the poll result connection details cannot be set to null.
    /// </summary>
    [Test]
    public void PollResultConnectionDetailsCannotBeSetToNull()
    {
      var test = new DatabaseConnectionPollResult("test");
      test.ConnectionDetails = null;
      Assert.IsNotNull(test.ConnectionDetails, "The connection details stayed null when they where set instead of resetting to something");
    }
  }
}

