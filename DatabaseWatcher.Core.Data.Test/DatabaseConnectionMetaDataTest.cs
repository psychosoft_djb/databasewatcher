﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Data.Test
{
  using System;
  using NUnit.Framework;

  /// <summary>
  /// Database connection meta data test.
  /// </summary>
  [TestFixture]
  public class DatabaseConnectionMetaDataTest
  {
    /// <summary>
    /// Test the constructor does not accept an unpopulated value for the name.
    /// </summary>
    [TestCase(null)]
    [TestCase("")]
    [TestCase("           ")]
    public void ConstructorThrowsArgumentExceptionForNullNameValue(string value)
    {
      Assert.Throws<ArgumentException>(() => new DatabaseConnectionMetaData(null), string.Format("No exception was thrown when [{0}] was given as the value name", value ?? "null"));
    }

    /// <summary>
    /// Test the name has trailing and leading spaces removed.
    /// </summary>
    [TestCase("test   ", "test")]
    [TestCase("   test", "test")]
    [TestCase("       test   ", "test")]
    public void NameValueHasTrailingAndLeadingSpacesRemoved(string name, string result)
    {
      var test = new DatabaseConnectionMetaData(name);
      Assert.IsTrue(test.Name.Equals(result), string.Format("Expected the meta data name to be [{0}] but it was [{1}].", result, test.Name));
    }

    /// <summary>
    /// Test if the default value is null.
    /// </summary>
    [Test]
    public void DefaultValueIsNull()
    {
      var test = new DatabaseConnectionMetaData("test");
      Assert.IsNull(test.Value, "Expected the default value of the Value field to be null and it wasn't");
    }
  }
}
