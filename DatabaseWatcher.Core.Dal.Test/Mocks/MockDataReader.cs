﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
using System.Diagnostics;

namespace DatabaseWatcher.Core.Dal.Test
{
  using System;
  using System.Data;

  /// <summary>
  /// Mock data reader.
  /// Used to loop through a read loop a given number of times.
  /// </summary>
  public class MockDataReader : IDataReader
  {
    /// <summary>
    /// Is this reader closed or not.
    /// </summary>
    private bool closed;

    /// <summary>
    /// The maximum read count.
    /// </summary>
    private readonly int maximumReadCount;

    /// <summary>
    /// The read count.
    /// </summary>
    private int readCount;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDataReader"/> class.
    /// </summary>
    /// <param name="maxReadCount">Maximum number of times Read can be called.</param>
    public MockDataReader(int maxReadCount)
    {
      this.maximumReadCount = maxReadCount;
      this.readCount = 0;
      this.closed = false;
    }

    /// <summary>
    /// Gets the <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDataReader"/> with the specified name.
    /// </summary>
    /// <param name="name">Name.</param>
    public object this[string name]
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    /// <summary>
    /// Gets the <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDataReader"/> with the specified i.
    /// </summary>
    /// <param name="i">The index.</param>
    public object this[int i]
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    /// <summary>
    /// Gets the depth.
    /// </summary>
    /// <value>The depth.</value>
    public int Depth
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    /// <summary>
    /// Gets the field count.
    /// </summary>
    /// <value>The field count.</value>
    public int FieldCount
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    /// <summary>
    /// Gets a value indicating whether this <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDataReader"/> is closed.
    /// </summary>
    /// <value><c>true</c> if is closed; otherwise, <c>false</c>.</value>
    public bool IsClosed
    {
      get
      {
        return this.closed;
      }
    }

    /// <summary>
    /// Gets the records affected.
    /// </summary>
    /// <value>The records affected.</value>
    public int RecordsAffected
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    /// <summary>
    /// Close this instance.
    /// </summary>
    public void Close()
    {
      this.closed = true;
    }

    /// <summary>
    /// Releases all resource used by the <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDataReader"/> object.
    /// </summary>
    /// <remarks>Call <see cref="Dispose"/> when you are finished using the
    /// <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDataReader"/>. The <see cref="Dispose"/> method leaves the
    /// <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDataReader"/> in an unusable state. After calling
    /// <see cref="Dispose"/>, you must release all references to the
    /// <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDataReader"/> so the garbage collector can reclaim the memory
    /// that the <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDataReader"/> was occupying.</remarks>
    public void Dispose()
    {
    }

    /// <summary>
    /// Gets the boolean.
    /// </summary>
    /// <returns><c>true</c>, if boolean was gotten, <c>false</c> otherwise.</returns>
    /// <param name="i">The index.</param>
    public bool GetBoolean(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the byte.
    /// </summary>
    /// <returns>The byte.</returns>
    /// <param name="i">The index.</param>
    public byte GetByte(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the bytes.
    /// </summary>
    /// <returns>The bytes.</returns>
    /// <param name="i">The index.</param>
    /// <param name="fieldOffset">Field offset.</param>
    /// <param name="buffer">Buffer.</param>
    /// <param name="bufferoffset">Bufferoffset.</param>
    /// <param name="length">Length.</param>
    public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the char.
    /// </summary>
    /// <returns>The char.</returns>
    /// <param name="i">The index.</param>
    public char GetChar(int i)
    {
      throw new NotImplementedException();
    }

    public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the data.
    /// </summary>
    /// <returns>The data.</returns>
    /// <param name="i">The index.</param>
    public IDataReader GetData(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the name of the data type.
    /// </summary>
    /// <returns>The data type name.</returns>
    /// <param name="i">The index.</param>
    public string GetDataTypeName(int i)
    {
      throw new NotImplementedException();
    }

    public DateTime GetDateTime(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the decimal.
    /// </summary>
    /// <returns>The decimal.</returns>
    /// <param name="i">The index.</param>
    public decimal GetDecimal(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the double.
    /// </summary>
    /// <returns>The double.</returns>
    /// <param name="i">The index.</param>
    public double GetDouble(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the type of the field.
    /// </summary>
    /// <returns>The field type.</returns>
    /// <param name="i">The index.</param>
    public Type GetFieldType(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the float.
    /// </summary>
    /// <returns>The float.</returns>
    /// <param name="i">The index.</param>
    public float GetFloat(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the GUID.
    /// </summary>
    /// <returns>The GUID.</returns>
    /// <param name="i">The index.</param>
    public Guid GetGuid(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the int16.
    /// </summary>
    /// <returns>The int16.</returns>
    /// <param name="i">The index.</param>
    public short GetInt16(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the int32.
    /// </summary>
    /// <returns>The int32.</returns>
    /// <param name="i">The index.</param>
    public int GetInt32(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the int64.
    /// </summary>
    /// <returns>The int64.</returns>
    /// <param name="i">The index.</param>
    public long GetInt64(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the name.
    /// </summary>
    /// <returns>The name.</returns>
    /// <param name="i">The index.</param>
    public string GetName(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the ordinal.
    /// </summary>
    /// <returns>The ordinal.</returns>
    /// <param name="name">Name.</param>
    public int GetOrdinal(string name)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the schema table.
    /// </summary>
    /// <returns>The schema table.</returns>
    public DataTable GetSchemaTable()
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the string.
    /// </summary>
    /// <returns>The string.</returns>
    /// <param name="i">The index.</param>
    public string GetString(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the value.
    /// </summary>
    /// <returns>The value.</returns>
    /// <param name="i">The index.</param>
    public object GetValue(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Gets the values.
    /// </summary>
    /// <returns>The values.</returns>
    /// <param name="values">Values.</param>
    public int GetValues(object[] values)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Ises the DBN ull.
    /// </summary>
    /// <returns><c>true</c>, if DBN ull was ised, <c>false</c> otherwise.</returns>
    /// <param name="i">The index.</param>
    public bool IsDBNull(int i)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Nexts the result.
    /// </summary>
    /// <returns><c>true</c>, if result was nexted, <c>false</c> otherwise.</returns>
    public bool NextResult()
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Read this instance.
    /// </summary>
    public bool Read()
    {
      var result = false;
      if (!this.IsClosed)
      {
        this.readCount++;
        result = this.readCount == this.maximumReadCount;
        Debug.Print(string.Format("Mock Data Read - count [{0}]", this.readCount));
      }

      return result;
    }
  }
}
