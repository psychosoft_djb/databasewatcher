﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Dal.Test
{
  using System.Data;
  using DatabaseWatcher.Core.Data;
  using NSubstitute;

  /// <summary>
  /// Mock database detail strategy.
  /// </summary>
  public class MockDatabaseDetailStrategy : IDatabaseDetailStrategy
  {
    /// <summary>
    /// The command return data.
    /// </summary>
    private readonly IDataReader commandReturnData;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDatabaseDetailStrategy"/> class.
    /// </summary>
    /// <param name="commandReturnData">Command return data.</param>
    public MockDatabaseDetailStrategy(IDataReader commandReturnData)
    {
      this.commandReturnData = commandReturnData;
    }

    /// <summary>
    /// Gets the connection detail form data.
    /// </summary>
    /// <returns>The connection detail form data.</returns>
    /// <param name="data">The data to read from.</param>
    public DatabaseConnectionDetail GetConnectionDetailFormData(IDataReader data)
    {
      return new DatabaseConnectionDetail();
    }

    /// <summary>
    /// Makes the connection detail command.
    /// </summary>
    /// <returns>The connection detail command.</returns>
    /// <param name="databaseConnection">Database connection.</param>
    public IDbCommand MakeConnectionDetailCommand(IDbConnection databaseConnection)
    {
      var command = Substitute.For<IDbCommand>();
      command.ExecuteReader().Returns(this.commandReturnData);
      return command;
    }

    /// <summary>
    /// Releases all resource used by the <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDatabaseDetailStrategy"/> object.
    /// </summary>
    /// <remarks>Call <see cref="Dispose"/> when you are finished using the
    /// <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDatabaseDetailStrategy"/>. The <see cref="Dispose"/> method
    /// leaves the <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDatabaseDetailStrategy"/> in an unusable state.
    /// After calling <see cref="Dispose"/>, you must release all references to the
    /// <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDatabaseDetailStrategy"/> so the garbage collector can
    /// reclaim the memory that the <see cref="T:DatabaseWatcher.Core.Dal.Test.MockDatabaseDetailStrategy"/> was occupying.</remarks>
    public void Dispose()
    {
      // nothing to do here, but this is part of the interface so its got to be implemented
    }
  }
}
