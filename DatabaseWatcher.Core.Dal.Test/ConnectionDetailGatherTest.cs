﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Dal.Test
{
  using System;
  using System.Data;
  using DatabaseWatcher.Core.Dal;
  using NSubstitute;
  using NUnit.Framework;

  /// <summary>
  /// Connection detail gather tests.
  /// </summary>
  [TestFixture]
  public class ConnectionDetailGatherTest
  {
    /// <summary>
    /// Test that a null argument exception is thrown with null connection in the constructor.
    /// </summary>
    [TestCase]
    public void NullArgumentExceptionIsThrownWithNullConnectionInTheConstructor()
    {
      Assert.Throws<ArgumentNullException>(() => new ConnectionDetailGather(null, Substitute.For<IDatabaseDetailStrategy>()), string.Format("No exception was thrown when a null database connection was given"));
    }

    /// <summary>
    /// Test that a null argument exception is thrown with null database detail visitor in the constructor.
    /// </summary>
    [TestCase]
    public void NullArgumentExceptionIsThrownWithNullDatabaseDetailVisitorInTheConstructor()
    {
      Assert.Throws<ArgumentNullException>(() => new ConnectionDetailGather(Substitute.For<IDbConnection>(), null), string.Format("No exception was thrown when a null detail strategy was given"));
    }

    /// <summary>
    /// Test that the databases connection details are not null.
    /// </summary>
    [TestCase]
    public void DatabaseConnectionDetailsAreNotNull()
    {
      var gather = new ConnectionDetailGather(Substitute.For<IDbConnection>(), Substitute.For<IDatabaseDetailStrategy>());

      Assert.IsNotNull(gather.GetConnectionDetails(), "The details from the connection gatherer are null and shouldn't be");
    }

    /// <summary>
    /// Test that the databases connection details have one result using mock objects.
    /// </summary>
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(10)]
    public void DatabaseConnectionDetailsHaveExpectedCountUsingMock(int expectedCount)
    {
      var connection = Substitute.For<IDbConnection>();         // wierd exception here with the subsitute for TODO work out whats causing the NSubstitute to blow up
      var detailsGather = new ConnectionDetailGather(connection, new MockDatabaseDetailStrategy(new MockDataReader(expectedCount)));
      var details = detailsGather.GetConnectionDetails();

      Assert.IsTrue(details.Count == expectedCount, string.Format("The number of records should have been {0} but it was {1}", expectedCount, details.Count));
    }

    /// <summary>
    /// Test that the databases connection details are closed when they have been read.
    /// </summary>
    [TestCase]
    public void DatabaseConnectionDetailsAreClosedWhenRead()
    {
      var connection = Substitute.For<IDbConnection>();
      var dataReader = new MockDataReader(1);
      var detailsGather = new ConnectionDetailGather(connection, new MockDatabaseDetailStrategy(dataReader));
      detailsGather.GetConnectionDetails();

      Assert.IsTrue(dataReader.IsClosed, "Expexected the reader to be closed after it had been finished with, but it wasn't.");
    }
  }
}
