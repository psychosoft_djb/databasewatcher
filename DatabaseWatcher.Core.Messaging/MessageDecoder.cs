﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Messaging
{
  using System;
  using System.IO;
  using System.Text;
  using System.Xml;
  using System.Xml.Serialization;

  /// <summary>
  /// Message decoder to extract UTF8 byte arrays to objects.
  /// </summary>
  public static class MessageDecoder
  {
    /// <summary>
    /// Decode the given byte array to an object.
    /// </summary>
    /// <returns>The decode object or a null object.</returns>
    /// <param name="encodedSource">The encoded source.</param>
    public static object Decode(byte[] encodedSource)
    {
      object result = null;
      var byteAsString = ConvertByteArrayToString(encodedSource);

      if ((!string.IsNullOrWhiteSpace(byteAsString)) && (!IsAllNull(byteAsString)))
      {
        var envelope = DecodeSerialisedMessageEnvelopeFromXml(byteAsString);
        if (envelope != null)
        {
          string message = DecodeFromBase64(envelope.Message);
          result = DecodeSerialisedXmlToObjectOfGivenType(envelope.MessageType, message);
        }
      }

      return result;
    }

    /// <summary>
    /// Decode a string from Base64.
    /// </summary>
    /// <returns>The string from base64.</returns>
    /// <param name="encoded">The Base64 encoded string.</param>
    private static string DecodeFromBase64(string encoded)
    {
      var data = Convert.FromBase64String(encoded ?? string.Empty);
      return Encoding.UTF8.GetString(data);
    }

    /// <summary>
    /// Decodes the serialised XML to object of a given type.
    /// </summary>
    /// <returns>The serialised XML in an object of given type or null.</returns>
    /// <param name="objectType">The object type to decode to.</param>
    /// <param name="xml">The XML to decode.</param>
    private static object DecodeSerialisedXmlToObjectOfGivenType(string objectType, string xml)
    {
      return DecodeSerialisedXmlToObjectOfGivenType(Type.GetType(objectType), xml);
    }

    /// <summary>
    /// Decodes the serialised XML to object of a given type.
    /// </summary>
    /// <returns>The serialised XML in an object of given type or null.</returns>
    /// <param name="objectType">The object type to decode to.</param>
    /// <param name="xml">The XML to decode.</param>
    private static object DecodeSerialisedXmlToObjectOfGivenType(Type objectType, string xml)
    {
      object result = null;
      using (var reader = new StringReader(xml))
      {
        using (var xmlReader = new XmlTextReader(reader))
        {
          var serialiser = new XmlSerializer(objectType);
          if (serialiser.CanDeserialize(xmlReader))
          {
            result = serialiser.Deserialize(xmlReader);
          }
        }
      }

      return result;
    }

    /// <summary>
    /// Decodes the serialised message envelope from XML.
    /// </summary>
    /// <returns>The message envelope from the XML or null.</returns>
    /// <param name="xml">The XML to decode.</param>
    private static MessageEnvelope DecodeSerialisedMessageEnvelopeFromXml(string xml)
    {
      MessageEnvelope result = null;
      if (IsXmlMessageEnvelope(xml))
      {
        result = (MessageEnvelope)DecodeSerialisedXmlToObjectOfGivenType(typeof(MessageEnvelope), xml);
      }

      return result;
    }

    /// <summary>
    /// Is the XML a message envelope.
    /// </summary>
    /// <returns><c>true</c>, if the XML is a message envelope, <c>false</c> otherwise.</returns>
    /// <param name="xml">The XML to check.</param>
    private static bool IsXmlMessageEnvelope(string xml)
    {
      var envelopeType = typeof(MessageEnvelope).Name;
      return envelopeType.Equals(GetObjectTypeFromXml(xml));
    }

    /// <summary>
    /// Gets the object type from XML.
    /// </summary>
    /// <returns>The object type from the root node of the XML or an empty string.</returns>
    /// <param name="xml">The XML to get the type from.</param>
    private static string GetObjectTypeFromXml(string xml)
    {
      const int EncodedObjectTypeNodeNumber = 1;
      var objectType = string.Empty;

      if (!string.IsNullOrWhiteSpace(xml))
      {
        var decodedXml = new XmlDocument();
        decodedXml.LoadXml(xml);
        if (decodedXml.HasChildNodes)
        {
          objectType = decodedXml.ChildNodes[EncodedObjectTypeNodeNumber].Name;
        }
      }

      return objectType;
    }

    /// <summary>
    /// Covert a byte array to a string.
    /// </summary>
    /// <returns>The array as a string.</returns>
    /// <param name="source">The source array.</param>
    private static string ConvertByteArrayToString(byte[] source)
    {
      var data = source ?? new byte[0];
      return Encoding.Default.GetString(data);
    }

    /// <summary>
    /// Test if a string is entirely UTF8 nulls.
    /// </summary>
    /// <returns><c>true</c>, if the string contains only UTF8 null, <c>false</c> otherwise.</returns>
    /// <param name="value">The value to test.</param>
    private static bool IsAllNull(string value)
    {
      const string Utf8Null = "\u0000";
      var stripped = value.Replace(Utf8Null, string.Empty);

      return string.IsNullOrWhiteSpace(stripped);
    }

  }
}