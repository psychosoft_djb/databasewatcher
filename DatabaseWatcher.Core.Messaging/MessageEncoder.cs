﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Messaging
{
  using System.IO;
  using System.Text;
  using System.Xml;
  using System.Xml.Serialization;

  /// <summary>
  /// Message encoder static class to encode to UTF8 byte arrays.
  /// </summary>
  public static class MessageEncoder
  {
    /// <summary>
    /// Encode the specified source to a UTF8 byte array.
    /// </summary>
    /// <param name="source">The source string to encode.</param>
    public static byte[] Encode(string source)
    {
      return GetByteArrayFromString(source);
    }

    /// <summary>
    /// Encode the specified source object to a UTF8 byte array.
    /// </summary>
    /// <param name="source">Source.</param>
    /// <returns>A byte array of the given object encoded to UTF8 XML, or an empty string.</returns>
    public static byte[] Encode(object source)
    {
      return Encode(EncodeToXMLString(MakeMessageEnvelope(source)));
    }

    /// <summary>
    /// Encodes an object to a XML String.
    /// </summary>
    /// <returns>The source object encoded as an XML string or null.</returns>
    /// <param name="source">The source object to encode.</param>
    private static string EncodeToXMLString(object source)
    {
      string xml = null;

      if (source != null)
      {
        var serialiser = new XmlSerializer(source.GetType());

        using (var stringWriter = new StringWriter())
        {
          using (var writer = XmlWriter.Create(stringWriter))
          {
            serialiser.Serialize(writer, source);
            xml = stringWriter.ToString();
          }
        }
      }

      return xml;
    }

    /// <summary>
    /// Makes the message envelope.
    /// </summary>
    /// <returns>The message envelope containing the message.</returns>
    /// <param name="message">The message to put in the envelope.</param>
    private static MessageEnvelope MakeMessageEnvelope(object message)
    {
      MessageEnvelope result = null;

      if (message != null)
      {
        var messageType = message.GetType();
        result = new MessageEnvelope();
        result.Message = EncodeToBase64(EncodeToXMLString(message));
        result.MessageType = messageType.FullName;
        result.MessageTypeAssembly = messageType.Assembly.FullName;
      }

      return result;
    }

    /// <summary>
    /// Encodes a string to base64.
    /// </summary>
    /// <returns>The string as base64.</returns>
    /// <param name="value">The value to encode.</param>
    private static string EncodeToBase64(string value)
    {
      return System.Convert.ToBase64String(GetByteArrayFromString(value));
    }

    /// <summary>
    /// Gets the a byte array from string.
    /// </summary>
    /// <returns>The byte array from string.</returns>
    /// <param name="value">The value to get the byte array from.</param>
    private static byte[] GetByteArrayFromString(string value)
    {
      return Encoding.UTF8.GetBytes(value ?? string.Empty);
    }
  }
}