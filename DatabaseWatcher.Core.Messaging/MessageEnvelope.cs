﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Messaging
{

  /// <summary>
  /// Message envelope to store an object and list its full type for encoding.
  /// </summary>
  public class MessageEnvelope
  {
    /// <summary>
    /// The message object.
    /// </summary>
    private string messageObject;

    /// <summary>
    /// The type of the message object.
    /// </summary>
    private string messageType;

    /// <summary>
    /// The name of the assembly for the message type.
    /// </summary>
    private string assemblyName;

    /// <summary>
    /// Gets or sets the type of the message object.
    /// </summary>
    /// <value>The type of the message object.</value>
    public string MessageType
    {
      get
      {
        return this.messageType;
      }

      set
      {
        this.messageType = value;
      }
    }

    /// <summary>
    /// Gets or sets the message type assembly.
    /// </summary>
    /// <value>The message type assembly.</value>
    public string MessageTypeAssembly
    {
      get
      {
        return this.assemblyName;
      }

      set
      {
        this.assemblyName = value;
      }
    }

    /// <summary>
    /// Gets or sets the message.
    /// </summary>
    /// <value>The message.</value>
    public string Message
    {
      get
      {
        return this.messageObject;
      }

      set
      {
        this.messageObject = value;
      }
    }
  }
}