﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Dal
{
  using System;
  using System.Collections.Generic;
  using System.Data;
  using DatabaseWatcher.Core.Data;

  /// <summary>
  /// Connection detail gather.
  /// </summary>
  public class ConnectionDetailGather
  {
    /// <summary>
    /// The connection to gather data from.
    /// </summary>
    private readonly IDbConnection connectionToGatherDataFrom;

    /// <summary>
    /// The detail strategy.
    /// </summary>
    private readonly IDatabaseDetailStrategy detailStrategy;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:DatabaseWatcher.Core.Dal.ConnectionDetailGather"/> class.
    /// </summary>
    /// <param name="connectionToGatherFrom">The database connection to gather data from.</param>
    /// <param name="detailStrategy">The strategy to use to when extracting data from the database connection.</param>
    /// <exception cref="ArgumentNullException">Thrown when either input variables are null.</exception>
    public ConnectionDetailGather(IDbConnection connectionToGatherFrom, IDatabaseDetailStrategy detailStrategy)
    {
      if ((connectionToGatherFrom == null) || (detailStrategy == null))
      {
        throw new ArgumentNullException(string.Format("The constructor input value for {0} is null", connectionToGatherFrom == null ? "connectionToGatherFrom" : "detailStrategy"));
      }

      this.connectionToGatherDataFrom = connectionToGatherFrom;
      this.detailStrategy = detailStrategy;
    }

    /// <summary>
    /// Gets the details of the connections on the database given in the constructor.
    /// </summary>
    /// <returns>The details of the open connections on the database.</returns>
    public List<DatabaseConnectionDetail> GetConnectionDetails()
    {
      var details = new List<DatabaseConnectionDetail>();
      var dataReader = this.GetConnectionDetailsDataReader();

      while (dataReader.Read())
      {
        details.Add(this.detailStrategy.GetConnectionDetailFormData(dataReader));
      }

      dataReader.Close();
      dataReader = null;

      return details;
    }

    /// <summary>
    /// Gets the connection details data reader.
    /// </summary>
    /// <returns>The connection details data reader.</returns>
    private IDataReader GetConnectionDetailsDataReader()
    {
      var command = this.detailStrategy.MakeConnectionDetailCommand(this.connectionToGatherDataFrom);
      return command.ExecuteReader();
    }
  }
}