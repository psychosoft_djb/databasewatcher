#Database watcher#

This is a rewrite of a tool I cobbled together some time ago to do live connection monitoring on a database.
In this version I'm separating the data collection from the analysis.  The data will be put on a RabbitMQ queue for processing elsewhere.

This won't be finished for a while as I'm not spending very much time on it right now.