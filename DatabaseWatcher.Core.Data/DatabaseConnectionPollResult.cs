﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Data
{
  using System;
  using System.Collections.Generic;

  /// <summary>
  /// A database connection poll result.
  /// </summary>
  public class DatabaseConnectionPollResult
  {
    /// <summary>
    /// The connection details.
    /// </summary>
    private IEnumerable<DatabaseConnectionDetail> connectionDetails;

    /// <summary>
    /// The name of the database.
    /// </summary>
    public readonly string DatabaseName;

    /// <summary>
    /// The time this result was taken.
    /// </summary>
    public readonly DateTime TimeTaken;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:DatabaseWatcher.Core.Data.DatabaseConnectionPollResult"/> class.
    /// </summary>
    /// <param name="databaseName">The database name this poll result is from.</param>
    public DatabaseConnectionPollResult(string databaseName)
    {
      if (string.IsNullOrWhiteSpace(databaseName))
      {
        throw new ArgumentException(string.Format("The given database name is {0}", databaseName == null ? "null" : "empty"));
      }

      this.DatabaseName = databaseName.Trim();
      this.TimeTaken = DateTime.Now;
    }

    /// <summary>
    /// Gets or sets the connection details.
    /// </summary>
    /// <value>The connection details.</value>
    public IEnumerable<DatabaseConnectionDetail> ConnectionDetails
    {
      get
      {
        if (this.connectionDetails == null)
        {
          this.connectionDetails = new List<DatabaseConnectionDetail>();
        }

        return this.connectionDetails as IEnumerable<DatabaseConnectionDetail>;
      }

      set { this.connectionDetails = value; }
    }
  }
}

