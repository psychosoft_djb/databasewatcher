﻿//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace DatabaseWatcher.Core.Data
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This class holds the details of a database connection.
    /// </summary>
    public class DatabaseConnectionDetail
  {
    /// <summary>
    /// The connection number.
    /// </summary>
    private long connectionNumber;

    /// <summary>
    /// The username.
    /// </summary>
    private string username;

    /// <summary>
    /// The meta data for the connection details.
    /// </summary>
    private IEnumerable<DatabaseConnectionMetaData> metaData;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:DatabaseWatcher.Core.Data.Connection"/> class.
    /// </summary>
    public DatabaseConnectionDetail()
    {
      this.CreatedDate = DateTime.Now;
      this.ConnectionNumber = 0;
      this.username = string.Empty;
    }

    /// <summary>
    /// Gets the created date.
    /// </summary>
    /// <value>The created date.</value>
    public DateTime CreatedDate { get; private set; }

    /// <summary>
    /// Gets or sets the connection number.
    /// </summary>
    /// <value>The connection number.</value>
    public long ConnectionNumber
    {
      get
      {
        return this.connectionNumber;
      }

      set
      {
        if (value >= 0)
        {
          this.connectionNumber = value;
        }
      }
    }

    /// <summary>
    /// Gets or sets the username.
    /// </summary>
    /// <value>The username.</value>
    public string Username
    {
      get
      {
        return this.username;
      }

      set
      {
        if (value != null)
        {
          this.username = value;
        }
      }
    }

    /// <summary>
    /// Gets or sets the connection meta data.
    /// </summary>
    /// <value>The connection meta data.</value>
    public IEnumerable<DatabaseConnectionMetaData> ConnectionMetaData
    {
      get
      {
        if (this.metaData == null)
        {
          this.metaData = new List<DatabaseConnectionMetaData>();
        }

        return this.metaData as IEnumerable<DatabaseConnectionMetaData>;
      }

      set { this.metaData = value; }
    }

  }
}
