﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Messaging.Test
{
  using NUnit.Framework;
  using NUnit.Framework.Internal;

  /// <summary>
  /// Message decoder tests.
  /// </summary>
  [TestFixture()]
  public class MessageDecoderTest
  {
    /// <summary>
    /// Test that null data decodes to null object.
    /// </summary>
    [Test]
    public void NullDataDecodesToNullObject()
    {
      Assert.IsNull(MessageDecoder.Decode(null), "Empty or null encoded byte array decoded to something and it shouldn't.");
    }

    /// <summary>
    /// Test that arrays of empty bytes decode to null object.
    /// </summary>
    /// <param name="encodedDataSize">Encoded data size.</param>
    [TestCase(0)]
    [TestCase(255)]
    public void EmptyDataDecodesToNullObject(int encodedDataSize)
    {
      var data = new byte[encodedDataSize];
      Assert.IsNull(MessageDecoder.Decode(data), "Empty byte array decoded to something and it shouldn't.");
    }

    /// <summary>
    /// Test that an encoded object decodes to the samething.
    /// </summary>
    [Test]
    public void EncodedObjectDecodes()
    {
      var encoded = MakeEncodedTestObject();
      var decoded = MessageDecoder.Decode(encoded);
      Assert.IsNotNull(decoded, "The decoded object was null but the input wasn't.");
    }
    /// <summary>
    /// Test that an encoded object decodes back to original values.
    /// </summary>
    [Test]
    public void EncodedObjectDecodesBackToOriginal()
    {
      var original = MakeTestObject();
      var encoded = MakeEncodedTestObject(original);
      var decoded = MessageDecoder.Decode(encoded);
      Assert.IsTrue(original.Equals(decoded), "The decoded object was not the same as the original");
    }
    /// <summary>
    /// Test that an encoded object decodes back to the same type of object.
    /// </summary>
    [Test]
    public void EncodedObjectDecodesBackToTheSameType()
    {
      var original = MakeTestObject();
      var encoded = MakeEncodedTestObject(original);
      var decoded = MessageDecoder.Decode(encoded);
      Assert.IsTrue(original.GetType().Equals(decoded.GetType()), string.Format("Expected an object of type {0} to be decoded but got {1}.", original.GetType().Name, decoded.GetType().Name));
    }

    /// <summary>
    /// Makes the test object.
    /// </summary>
    /// <returns>The test object.</returns>
    private static MessageTestObject MakeTestObject()
    {
      return new MessageTestObject() { number = 123, value = "test & stuff <thing>" };
    }

    /// <summary>
    /// Make a test object encoded as XML.
    /// This relys on the MessageEncoder actually working properly.
    /// </summary>
    /// <returns>The encoded test object.</returns>
    /// <param name="toEncode">The object to encode, uses MakeTestObject if its null.</param>
    private static byte[] MakeEncodedTestObject(object toEncode = null)
    {
      return MessageEncoder.Encode(toEncode ?? MakeTestObject());
    }
  }
}
