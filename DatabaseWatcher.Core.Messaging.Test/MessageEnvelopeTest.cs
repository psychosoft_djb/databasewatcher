﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Messaging.Test
{
  using NUnit.Framework;

  /// <summary>
  /// Message envelope tests.
  /// </summary>
  [TestFixture]
  public class MessageEnvelopeTest
  {
    /// <summary>
    /// Test that a new envelope has a null message.
    /// </summary>
    [Test]
    public void NewEnvelopeHasNullMessage()
    {
      var test = new MessageEnvelope();
      Assert.IsNull(test.Message, "The envelope does not have a null message");
      Assert.IsNull(test.MessageType, string.Format("The envelope thinks the message type is {0} instead of null", test.MessageType));
    }

    /// <summary>
    /// Test the messages type is not changed by changing message.
    /// </summary>
    /// <param name="messageType">Message type.</param>
    /// <param name="message">Message.</param>
    [TestCase(null, "test")]
    [TestCase("string", null)]
    public void MessageTypeIsNotChangedByChangingMessage(string messageType, string message)
    {
      var test = new MessageEnvelope() { MessageType = messageType };
      test.Message = message;
      Assert.IsTrue(string.Equals(messageType, test.MessageType), string.Format("The message type changed from {0} to {1} when a message was set", messageType, test.MessageType));
    }

    /// <summary>
    /// Test the messages assembly type is not changed by changing other values.
    /// </summary>
    [Test]
    public void MessageAssemblyTypeIsNotChangedByChangingOtherValues()
    {
      var test = new MessageEnvelope();
      Assert.IsNull(test.MessageTypeAssembly, "The message assembly type was not null");

      test.Message = "test";
      Assert.IsNull(test.MessageTypeAssembly, "The message assembly type was not null after setting the message");

      test.MessageType = "test";
      Assert.IsNull(test.MessageTypeAssembly, "The message assembly type was not null after setting the message type");
    }
  }
}
