﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2016, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Messaging.Test
{
  using System.Text;
  using System.Xml;

  using System.Collections;
  using NUnit.Framework;

  /// <summary>
  /// Message encoder tests.
  /// </summary>
  [TestFixture()]
  public class MessageEncoderTest
  {
    /// <summary>
    /// Test that a string encodes to correct bytes.
    /// </summary>
    /// <param name="source">The value to test the encoding with.</param>
    [TestCase("")]
    [TestCase("test things")]
    [TestCase("123 >< &&")]
    public void StringEncodesToCorrectBytes(string source)
    {
      var result = MessageEncoder.Encode(source);
      var expected = Encoding.UTF8.GetBytes(source);

      Assert.IsNotNull(result, string.Format("The expected result for encoding the string {0} was null and shouldn't be.", source));
      Assert.AreEqual(expected, result, string.Format("The expected result for encoding the string {0} did not match the encoded one.", source));
    }

    /// <summary>
    /// Test that a null string encodes to an empty byte array.
    /// </summary>
    [TestCase]
    public void NullStringEncodesToAnEmptyByteArray()
    {
      var result = MessageEncoder.Encode((string)null);

      Assert.IsNotNull(result, "A null string was returned when encoding null");
      Assert.IsTrue(result.Length == 0, string.Format("The returned byte array for a null string should have been 0 in lenght but it is {0}", result.Length));
    }

    /// <summary>
    /// Test that a null objects encode to an empty byte array.
    /// </summary>
    /// <param name="source">Source.</param>
    [TestCase(null)]
    [TestCase((ArrayList)null)]
    [TestCase((byte[])null)]
    public void NullObjectsEncodeToAnEmptyByteArray(object source)
    {
      var result = MessageEncoder.Encode(source);

      Assert.IsTrue(result.Length == 0, string.Format("The returned byte array for a null object should have been 0 in lenght but it is {0}", result.Length));
    }

    /// <summary>
    /// Test that an object is serialised to xml.
    /// </summary>
    [Test]
    public void ObjectIsSerialisedToXML()
    {
      var data = new MessageTestObject() { number = 1, value = "&&&&" };
      var encode = MessageEncoder.Encode(data);
      var stringValue = Encoding.Default.GetString(encode);
      var xml = new XmlDocument();
      xml.LoadXml(stringValue);
    }

    /// <summary>
    /// Tests the serialise to XML Removes special characters.
    /// </summary>
    [TestCase("&&&&&")]
    [TestCase(">>>>>")]
    [TestCase("<<<<<")]
    public void SerialiseToXMLRemovesSpecialCharacters(string testValue)
    {
      var data = new MessageTestObject() { number = 1, value = testValue };
      var encode = MessageEncoder.Encode(data);
      var stringValue = Encoding.Default.GetString(encode);
      Assert.IsFalse(stringValue.Contains(testValue));
    }
  }
}
