﻿//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

namespace DatabaseWatcher.Core.Messaging.Test
{
  /// <summary>
  /// POCO for Message testing.
  /// </summary>
  public class MessageTestObject
  {
    /// <summary>
    /// Gets or sets the number.
    /// </summary>
    /// <value>The number.</value>
    public int number { get; set; }

    /// <summary>
    /// Gets or sets the value.
    /// </summary>
    /// <value>The value.</value>
    public string value { get; set; }

    /// <summary>
    /// Determines whether the specified <see cref="DatabaseWatcher.Core.Messaging.Test.MessageTestObject"/> is
    /// equal to the current <see cref="T:DatabaseWatcher.Core.Messaging.Test.MessageTestObject"/>.
    /// </summary>
    /// <param name="toTest">The <see cref="DatabaseWatcher.Core.Messaging.Test.MessageTestObject"/> to compare with the current <see cref="T:DatabaseWatcher.Core.Messaging.Test.MessageTestObject"/>.</param>
    /// <returns><c>true</c> if the specified <see cref="DatabaseWatcher.Core.Messaging.Test.MessageTestObject"/> is equal to
    /// the current <see cref="T:DatabaseWatcher.Core.Messaging.Test.MessageTestObject"/>; otherwise, <c>false</c>.</returns>
    public bool Equals(MessageTestObject toTest)
    {
      var result = false;

      if (toTest != null)
      {
        result = (toTest.number == number) && (value.Equals(toTest.value, System.StringComparison.InvariantCulture));
      }

      return result;
    }
  }
}
